FROM alpine AS builder

ARG VERSION

WORKDIR /home/node/
RUN apk add git npm build-base cmake openssl-dev openssl-libs-static
RUN git clone --depth 1 --branch ${VERSION} https://github.com/webtorrent/bittorrent-tracker.git . && \
    rm -rf .git \
           .github \
           .gitignore \
           .npmignore \
           examples \
           img \
           test \
           tools \
           .travis.yml \
           *.md \
           LICENSE
RUN npm install --omit=dev && \
    npm cache clean --force
RUN mkdir -p node-datachannel/build/Release/ && \
    mv node_modules/node-datachannel/package.json node_modules/node-datachannel/lib node-datachannel/ && \
    mv node_modules/node-datachannel/build/Release/node_datachannel.node node-datachannel/build/Release/ && \
    rm -rf node_modules/node-datachannel/ && \
    mv node-datachannel node_modules/
COPY entrypoint.sh .

FROM alpine
ENV HTTP=1 \
    UDP=1 \
    WS=1 \
    QUIET=1 \
    SILENT=0 \
    TRUST_PROXY=0 \
    STATS=1 \
    INTERVAL=600000 \
    PORT=8000
WORKDIR /home/node/
RUN apk add npm
COPY --from=builder /home/node/ .
ENTRYPOINT ["/home/node/entrypoint.sh"]
